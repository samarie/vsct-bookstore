'use strict';

describe('User controller', function() {
	var createConnectedUserController, createDisconnectedUserController;
	var user = {username: 'supertoto', firstname: 'toto', lastname: 'tata'};

	beforeEach(module('bookstore-user'));

	beforeEach(inject(function($rootScope, $controller) {
		createDisconnectedUserController = function() {
			return $controller('UserCtrl', {
				$scope: $rootScope.$new(),
				userInfos : null
			});
		};

		createConnectedUserController = function() {
			return $controller('UserCtrl', {
				$scope: $rootScope.$new(),
				userInfos : user
			});
		};
	}));

	describe('initialize', function() {
		it('should init variables with disconnected user', function() {
			// given

			// when
			var ctrl = createDisconnectedUserController();

			// then
			expect(ctrl.connectedUserInfos).toBe(null);
			expect(ctrl.getInfosInProgress).toBe(false);
			expect(ctrl.error).toEqual('Veuillez vous connecter ou rafraichir la page');
		});

		it('should init variables with connected user', function() {
			// given

			// when
			var ctrl = createConnectedUserController();

			// then
			expect(ctrl.connectedUserInfos).toBe(user);
			expect(ctrl.getInfosInProgress).toBe(false);
			expect(ctrl.error).toBe(null);
		});
	});

	describe('broadcast actions', function() {
		beforeEach(inject(function($q, UserService) {
			spyOn(UserService, 'getUserInfos').andCallFake(function() {
				var defer = $q.defer();
				defer.resolve(user);
				return defer.promise;
			});
		}));

		it('should reset flag and update user infos on login_success', inject(function($rootScope, UserService) {
			// given
			var ctrl = createDisconnectedUserController();

			// when
			$rootScope.$broadcast('login_success');
			expect(ctrl.getInfosInProgress).toBe(true);
			$rootScope.$digest();

			// then
			expect(UserService.getUserInfos).toHaveBeenCalled();
			expect(ctrl.connectedUserInfos).toBe(user);
			expect(ctrl.getInfosInProgress).toBe(false);
			expect(ctrl.error).toBe(null);
		}));

		it('should reset flag and infos on logout_success', inject(function($rootScope) {
			// given
			var ctrl = createConnectedUserController();

			// when
			$rootScope.$broadcast('logout_success');

			// then
			expect(ctrl.connectedUserInfos).toBe(null);
			expect(ctrl.getInfosInProgress).toBe(false);
			expect(ctrl.error).toEqual('Veuillez vous connecter');
		}));
	})
});