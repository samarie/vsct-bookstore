'use strict';

describe('User service', function() {
	var $httpBackend;

	beforeEach(module('bookstore-user'));

	beforeEach(inject(function($injector, NotificationService) {
		$httpBackend = $injector.get('$httpBackend');
		spyOn(NotificationService, 'error').andCallThrough();
	}));

	describe('getUserInfos', function() {
		it('should return user infos', inject(function($rootScope, $httpBackend, RestURLs, UserService) {
			// given
			var user = {username: 'supertoto', firstname: 'toto', lastname: 'tata'};
			var result = null;

			$httpBackend.expectGET(RestURLs.userInfosUrl).respond(200, user);

			// when
			UserService.getUserInfos()
				.then(function(infos) {
					result = infos;
				});
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect(result.username).toEqual(user.username);
			expect(result.firstname).toEqual(user.firstname);
			expect(result.lastname).toEqual(user.lastname);
		}));

		it('should return null on rest error', inject(function($rootScope, $httpBackend, RestURLs, UserService) {
			// given
			var result = null;

			$httpBackend.expectGET(RestURLs.userInfosUrl).respond(500);

			// when
			UserService.getUserInfos()
				.then(function(infos) {
					result = infos;
				});
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect(result).toBe(null);
		}));
	});
});