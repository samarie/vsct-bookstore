'use strict';

describe('Login controller', function() {
	var createConnectedUserController, createDisconnectedUserController;

	beforeEach(module('bookstore-login'));

	beforeEach(inject(function($rootScope, $controller) {
		createDisconnectedUserController = function() {
			return $controller('LoginCtrl', {
				$scope: $rootScope.$new(),
				connectedUser : null
			});
		};

		createConnectedUserController = function() {
			return $controller('LoginCtrl', {
				$scope: $rootScope.$new(),
				connectedUser : 'username'
			});
		};
	}));

	describe('initialize', function() {
		it('should init variables with disconnected user', function() {
			// given

			// when
			var ctrl = createDisconnectedUserController();

			// then
			expect(ctrl.user).toEqual({});
			expect(ctrl.loginInProgress).toBe(false);
			expect(ctrl.connectedUser).toBe(null);
		});

		it('should init variables with connected user', function() {
			// given

			// when
			var ctrl = createConnectedUserController();

			// then
			expect(ctrl.user).toEqual({});
			expect(ctrl.loginInProgress).toBe(false);
			expect(ctrl.connectedUser).toEqual('username');
		});
	});

	describe('broadcast actions', function() {
		it('should reset form and flags, and set username on login_success', inject(function($rootScope) {
			// given
			var ctrl = createDisconnectedUserController();
			ctrl.loginInProgress = true;
			ctrl.user = {username: 'toto', password: 'tata'};

			// when
			$rootScope.$broadcast('login_success', 'toto');

			// then
			expect(ctrl.user).toEqual({});
			expect(ctrl.loginInProgress).toBe(false);
			expect(ctrl.connectedUser).toEqual('toto');
		}));

		it('should update flag on login_fail', inject(function($rootScope) {
			// given
			var ctrl = createDisconnectedUserController();
			ctrl.loginInProgress = true;
			ctrl.user = {username: 'toto', password: 'tata'};

			// when
			$rootScope.$broadcast('login_fail');

			// then
			expect(ctrl.user).toEqual({username: 'toto', password: 'tata'});
			expect(ctrl.loginInProgress).toBe(false);
			expect(ctrl.connectedUser).toEqual(null);
		}));

		it('should reset username on logout_success', inject(function($rootScope) {
			// given
			var ctrl = createConnectedUserController();

			// when
			$rootScope.$broadcast('logout_success');

			// then
			expect(ctrl.connectedUser).toBe(null);
		}));
	})

	describe('logout', function() {

		beforeEach(inject(function(LoginService) {
			spyOn(LoginService, 'check').andCallThrough();
			spyOn(LoginService, 'login').andCallFake(function(){});
		}));

		it('should call check input service only and update flag when input is incorrect', inject(function(LoginService) {
			// given
			var ctrl = createDisconnectedUserController();
			ctrl.user = {username: 'toto'};

			// when
			ctrl.login();

			// then
			expect(LoginService.check).toHaveBeenCalledWith({username: 'toto'});
			expect(ctrl.loginInProgress).toBe(false);
		}));

		it('should update flag, call check input service and login service when input is correct', inject(function(LoginService) {
			// given
			var ctrl = createDisconnectedUserController();
			ctrl.user = {username: 'toto', password: 'tata'};

			// when
			ctrl.login();

			// then
			expect(ctrl.loginInProgress).toBe(true);
			expect(LoginService.check).toHaveBeenCalledWith({username: 'toto', password: 'tata'});
			expect(LoginService.login).toHaveBeenCalledWith({username: 'toto', password: 'tata'});
		}));
	});

	describe('logout', function() {
		beforeEach(inject(function(LoginService) {
			spyOn(LoginService, 'logout').andCallFake(function(){});
		}));

		it('should call logout service', inject(function(LoginService) {
			// given
			var ctrl = createConnectedUserController();
			ctrl.user = {username: 'toto', password: 'tata'};

			// when
			ctrl.logout();

			// then
			expect(LoginService.logout).toHaveBeenCalled();
		}));
	});
});