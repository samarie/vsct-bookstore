'use strict';

describe('Login service', function() {
	var $httpBackend;

	beforeEach(module('bookstore-login'));

	beforeEach(inject(function(NotificationService) {
		spyOn(NotificationService, 'error').andCallThrough();
		spyOn(NotificationService, 'info').andCallThrough();
	}));

	describe('check', function() {
		it('should return true', inject(function(LoginService) {
			// given
			var user = {username: 'toto', password: 'tata'};

			// when
			var inputIsCorrect = LoginService.check(user);

			// then
			expect(inputIsCorrect).toBe(true);
		}));

		it('should display error and return false when password is missing', inject(function(LoginService, NotificationService) {
			// given
			var user = {username: 'toto'};

			// when
			var inputIsCorrect = LoginService.check(user);

			// then
			expect(inputIsCorrect).toBe(false);
			expect(NotificationService.error).toHaveBeenCalledWith('Veuillez entrer votre mot de passe');
		}));

		it('should display error and return false when password is empty', inject(function(LoginService, NotificationService) {
			// given
			var user = {username: 'toto', password: ''};

			// when
			var inputIsCorrect = LoginService.check(user);

			// then
			expect(inputIsCorrect).toBe(false);
			expect(NotificationService.error).toHaveBeenCalledWith('Veuillez entrer votre mot de passe');
		}));

		it('should display error and return false when username is missing', inject(function(LoginService, NotificationService) {
			// given
			var user = {password: 'tata'};

			// when
			var inputIsCorrect = LoginService.check(user);

			// then
			expect(inputIsCorrect).toBe(false);
			expect(NotificationService.error).toHaveBeenCalledWith('Veuillez entrer votre username');
		}));

		it('should display error and return false when username is missing', inject(function(LoginService, NotificationService) {
			// given
			var user = {username: '', password: 'tata'};

			// when
			var inputIsCorrect = LoginService.check(user);

			// then
			expect(inputIsCorrect).toBe(false);
			expect(NotificationService.error).toHaveBeenCalledWith('Veuillez entrer votre username');
		}));
	});

	describe('login', function() {

		beforeEach(inject(function($rootScope, $injector) {
			$httpBackend = $injector.get('$httpBackend');

			spyOn($rootScope, '$broadcast').andCallThrough();
		}));

		it('should broadcast login_success and show notification on login success', inject(function ($rootScope, NotificationService, LoginService, RestURLs) {
			// given
			var user = {username: 'toto', password: 'tata'};
			$httpBackend.expectPOST(RestURLs.sessionUrl, user)
				.respond(200, {username: 'toto'});

			// when
			LoginService.login(user);
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect($rootScope.$broadcast).toHaveBeenCalledWith('login_success', 'toto');
			expect(NotificationService.info).toHaveBeenCalledWith('Vous êtes maintenant authentifié');
		}));

		it('should broadcast login_fail and show notification on login fail', inject(function ($rootScope, NotificationService, LoginService, RestURLs) {
			// given
			var user = {username: 'toto', password: 'tata'};
			$httpBackend.expectPOST(RestURLs.sessionUrl, user)
				.respond(404);

			// when
			LoginService.login(user);
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect($rootScope.$broadcast).toHaveBeenCalledWith('login_fail');
			expect(NotificationService.error).toHaveBeenCalledWith('Authentification échouée');
		}));
	});

	describe('logout', function() {

		beforeEach(inject(function($rootScope, $injector) {
			$httpBackend = $injector.get('$httpBackend');

			spyOn($rootScope, '$broadcast').andCallThrough();
		}));

		it('should broadcast logout_success and show notification on logout success', inject(function ($rootScope, NotificationService, LoginService, RestURLs) {
			// given
			$httpBackend.expectDELETE(RestURLs.sessionUrl)
				.respond(200);

			// when
			LoginService.logout();
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect($rootScope.$broadcast).toHaveBeenCalledWith('logout_success');
			expect(NotificationService.info).toHaveBeenCalledWith('Vous êtes maintenant déconnecté');
		}));

		it('should show notification on logout fail', inject(function ($rootScope, NotificationService, LoginService, RestURLs) {
			// given
			$httpBackend.expectDELETE(RestURLs.sessionUrl)
				.respond(500);

			// when
			LoginService.logout();
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect(NotificationService.error).toHaveBeenCalledWith('Déconnexion échouée');
		}));
	});

	describe('getConnectedUser', function() {

		beforeEach(inject(function($injector) {
			$httpBackend = $injector.get('$httpBackend');
		}));

		it('should return username on GET success', inject(function ($rootScope, LoginService, RestURLs) {
			// given
			var username = null;
			$httpBackend.expectGET(RestURLs.sessionUrl)
				.respond(200, {username: 'toto'});

			// when
			LoginService.getConnectedUser()
				.then(function(restUsername) {
					username = restUsername;
				});
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect(username).toEqual('toto');
		}));

		it('should return null on GET fail', inject(function ($rootScope, LoginService, RestURLs) {
			// given
			var username = '';
			$httpBackend.expectGET(RestURLs.sessionUrl)
				.respond(401);

			// when
			LoginService.getConnectedUser()
				.then(function(restUsername) {
					username = restUsername;
				});
			$httpBackend.flush();
			$rootScope.$digest();

			// then
			expect(username).toBe(null);
		}));
	});
});