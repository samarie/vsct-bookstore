'use strict';

angular
	.module('bookstoreApp', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'ui.router',
		'bookstore-login',
		'bookstore-books',
		'bookstore-user'
	])
	.config(['$httpProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', function ($httpProvider, $locationProvider, $stateProvider, $urlRouterProvider) {
		$httpProvider.defaults.withCredentials = true;

		$stateProvider
			.state('menu', {
				url: '/menu',
				abstract: true,
				templateUrl: 'views/navbar.html',
				controller: 'LoginCtrl',
				controllerAs: 'ctrl'
			})
			.state('menu.home', {
				url: '/home',
				templateUrl: 'views/main.html',
				controller: 'BooksCtrl',
				controllerAs: 'booksCtrl'
			})
			.state('menu.user', {
				url: '/user',
				templateUrl: 'views/user.html',
				controller: 'UserCtrl',
				controllerAs: 'userCtrl',
				resolve: {
					userInfos: ['UserService', function (UserService) {
						return UserService.getUserInfos();
					}]
				}
			});

		$urlRouterProvider.otherwise('/menu/home');
	}])
;
