'use strict';

(function() {
	angular.module('bookstore-books', ['bookstore-rest', 'bookstore-notification','bookstore-login']);
})();
