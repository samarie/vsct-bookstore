'use strict';

(function () {

	function BooksService(RestBooksService, $log) {
		return {
			getAllBooks : function () {
				var books = RestBooksService.books().$promise;
				$log.debug("Call book list",books);
				return books;
			}
		}
	}

	angular.module('bookstore-books')
		.factory('BooksService', BooksService);

})();
