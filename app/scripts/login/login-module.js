'use strict';

(function() {
	angular.module('bookstore-login', ['bookstore-rest', 'bookstore-notification']);
})();
