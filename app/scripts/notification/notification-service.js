'use strict';

(function() {
	function NotificationService() {
		this.info = function(message) {
			alert(message);
		};

		this.error = function(message) {
			// TODO quand on aura une gestion d'erreur
			alert(message);
		};
	}

	angular.module('bookstore-notification')
		.service('NotificationService', NotificationService);
})();