'use strict';

(function() {

	function RestUserService($resource, RestURLs) {
		return $resource(RestURLs.userInfosUrl, {}, {
			getInfos: 	{method: 'GET',		withCredentials: true}
		});
	}

	function RestBooksService($resource, RestURLs) {
		return $resource(RestURLs.bookListUrl, {}, {
			books: 	{method: 'GET',		withCredentials: true}
		});
	}

	angular.module('bookstore-rest')
		.factory('RestBooksService', RestBooksService)
		.factory('RestUserService', RestUserService)
		.factory('RestLoginService', RestLoginService);
})();
