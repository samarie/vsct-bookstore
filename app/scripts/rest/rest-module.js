'use strict';

(function() {
	var server = 'http://127.0.0.1:8080/MockRestServer';
	angular.module('bookstore-rest', ['ngResource'])
		.constant('RestURLs', {
			serverUrl:			server,
			sessionUrl:			server + '/user/session',
			userInfosUrl:		server + '/user/infos',
			bookListUrl:        server + '/books/list'
		});
})();
