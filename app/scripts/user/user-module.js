'use strict';

(function() {
	angular.module('bookstore-user', ['bookstore-rest', 'bookstore-notification']);
})();