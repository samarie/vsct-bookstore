'use strict';

(function() {
	function UserCtrl($scope, UserService, userInfos) {
		var vm = this;

		vm.updateInfos = function(infos) {
			vm.getInfosInProgress = false;
			vm.connectedUserInfos = infos;
			if(infos === null) {
				vm.error = 'Veuillez vous connecter ou rafraichir la page';
			}
			else {
				vm.error = null;
			}
		};

		vm.refreshUserInfos = function() {
			vm.getInfosInProgress = true;
			UserService.getUserInfos()
				.then(function(infos) {
					vm.updateInfos(infos);
				});
		};

		$scope.$on('login_success', function() {
			vm.refreshUserInfos();
		});

		$scope.$on('logout_success', function() {
			vm.updateInfos(null);
			vm.error = 'Veuillez vous connecter';
		});

		vm.updateInfos(userInfos);
	}

	angular.module('bookstore-user')
		.controller('UserCtrl', UserCtrl);
})();