'use strict';

(function() {

	function UserService(RestUserService, NotificationService) {
		this.getUserInfos = function() {
			return RestUserService.getInfos().$promise
				.then(function(infos) {
					return infos;
				})
				.catch(function(error) {
					if(error.status === 401) {
						NotificationService.error('Veuillez vous connecter');
					}
					else {
						NotificationService.error('Une erreur est survenue lors de la récupération des infos utilisateur');
					}
					return null;
				});
		};
	}

	angular.module('bookstore-user')
		.service('UserService', UserService);
})();