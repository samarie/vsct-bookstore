# VSCT Bookstore #

Ce projet a été développé pour un coding DOJO chez VSCT. Il vise à faire découvrir AngularJS à des développeurs ayant des connaissances javascript mais débutant en AngularJS.
Le format court (2h) permettra uniquement de faire découvrir quelques concepts.

## AngularJS ##

Documentation officielle : https://docs.angularjs.org/api
Style guide de Todd Motto : https://github.com/toddmotto/angularjs-styleguide

## Déroulement du DOJO ##

### Partie 1 ###

La première partie de 15-20 min vise à présenter AngularJS avec un live coding exposant le double binding

1. Présentation succincte d'AngularJS
2. Templates HTML - Module - Controller - Service - directives
3. ng-app
4. Routeur (url - controller - template HTML)
5. Injection de dépendance

### Partie 2 ###

La seconde partie : hand's on visant à coder le login

1. Objectif : au clic du bouton de login, afficher le login et mot de passe en alert
2. Objectif : créer une factory RestLoginService permettant d'appeler 3 services REST sur l'url RestURLs.sessionUrl (login, isConnected, logout). La factory sera dans le module bookstore-rest.
Utiliser $resource
3. Objectifs : 
Créer le service LoginService exposant 2 méthodes : checkInput et login. 
Appeler les méthodes du service à partir du controller pour réaliser le login.
Afficher Bonjour <username> au login.

Bonus : Faire le logout (rapide).

### Partie 3 ###

S'il reste du temps : présentation de ng-repeat et des filters.